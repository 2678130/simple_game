package ui;/*
Thierry Bergeron
Date : 2019-11-18
Description :
Nom du Fichier : Controller.java
*/

import javax.swing.*;
import java.awt.*;

public class MainPanel extends JPanel {

    public MainPanel(BoardPanel boardPanel, HealthPanel healthPanel, ScorePanel scorePanel, JButton startButton, JButton stopButton) {
        // --- Visual Objects ---
        this.setLayout(null);

        this.add(boardPanel, 1, 0);
        boardPanel.setSize(PanelProperties.GAME_BOARD_WIDTH,PanelProperties.GAME_BOARD_HEIGHT);
        boardPanel.setLocation(18, 45);
        boardPanel.setBackground(Color.black);

        this.add(healthPanel);
        healthPanel.setSize(170, 20);
        healthPanel.setLocation(397, 5);
        healthPanel.setBackground(Color.red);

        this.add(scorePanel);
        scorePanel.setSize(170, 40);
        scorePanel.setLocation(18, 5);



        this.add(startButton, 3, 0);
        startButton.setText("Start");
        startButton.setLocation(235, 200);
        startButton.setSize(120, 60);

        this.add(stopButton, 2, 0);
        stopButton.setText("Stop");
        stopButton.setSize(90, 30);
        stopButton.setLocation(243, 0);
        stopButton.setVisible(false);

    }
}
