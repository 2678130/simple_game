/*
Thierry Bergeron
Date : 2019-11-20
Description :
Nom du Fichier : GameBoard.java
*/
package ui;

import objects.Player;
import objects.Trap;
import objects.Treasure;
import objects.Villain;
import images.GameImages;

import javax.swing.*;
import java.awt.*;

public class BoardPanel extends JPanel {
    //Images
    private Image backgroundImage;
    private Image playerImage;
    private Image villainRedImage;
    private Image treasureImage;
    private Image trapImageOpen;
    private Image trapImageClosed;

    //Reference objects
    private Villain[] villain;
    private Player player;
    private Treasure treasure;
    private Trap[] trap;


    public BoardPanel(Player player, Villain[] villain, Treasure treasure, Trap[] trap) {
        //set initial background image
        ImageIcon iconBackgroundImage = new ImageIcon(GameImages.LEVEL1_IMAGE_SRC_PATH);
        this.backgroundImage = iconBackgroundImage.getImage();

        //set initial player image
        ImageIcon iconPlayerImage = new ImageIcon(GameImages.PLAYER_IMAGE_SRC_PATH);
        this.playerImage = iconPlayerImage.getImage();

        //set villainRedImage
        ImageIcon iconVillainRedImage = new ImageIcon(GameImages.VILLAIN_IMAGE_SRC_PATH);
        this.villainRedImage = iconVillainRedImage.getImage();

        //set treasure image
        ImageIcon iconTreasureImage = new ImageIcon(GameImages.TREASURE_IMAGE_SRC_PATH);
        this.treasureImage = iconTreasureImage.getImage();

        //set trap image open
        ImageIcon iconTrapImageOpen = new ImageIcon(GameImages.TRAP_OPEN_IMAGE_SRC_PATH);
        this.trapImageOpen = iconTrapImageOpen.getImage();

        ImageIcon iconTrapImageClosed = new ImageIcon(GameImages.TRAP_CLOSED_IMAGE_SRC_PATH);
        this.trapImageClosed = iconTrapImageClosed.getImage();

        this.player = player;
        this.villain = villain;
        this.treasure = treasure;
        this.trap = trap;
    }

    /**
     * Paint background
     * @param g graphics
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        paintBackground(g);
        Toolkit.getDefaultToolkit().sync();
    }

    /**
     * Paint Background function
     *
     * @param g graphics
     */
    private void paintBackground(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.drawImage(this.backgroundImage, PanelProperties.BACKGROUND_IMAGE_INITIAL_POSITION_X, PanelProperties.BACKGROUND_IMAGE_INITIAL_POSITION_Y, this);
    }

    /**
     * Paint all children (Player, Villain, Traps, Treasure)
     * @param g graphics
     */
    public void paintChildren(Graphics g) {
        super.paintChildren(g);
        paintObject(g);
        Toolkit.getDefaultToolkit().sync();
    }

    /**
     * Paint game objects function
     *
     * @param g graphics
     */
    private void paintObject(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;

        //paint player
        graphics2D.drawImage(this.playerImage, this.player.getPositionX(), this.player.getPositionY(), this);

        //paint villains
        for (int i = 0; i < this.villain.length; i++) {
            graphics2D.drawImage(this.villainRedImage, this.villain[i].getX(), this.villain[i].getY(), this);
        }

        //paint traps
        for (int i = 0; i < this.trap.length; i++) {
            if (this.trap[i].isCollided()) {
                graphics2D.drawImage(this.trapImageClosed, this.trap[i].getPositionX(), this.trap[i].getPositionY(), this);
            } else {
                graphics2D.drawImage(this.trapImageOpen, this.trap[i].getPositionX(), this.trap[i].getPositionY(), this);
            }
        }

        //paint treasure
        graphics2D.drawImage(this.treasureImage, this.treasure.getPositionX(), this.treasure.getPositionY(), this);
    }
}