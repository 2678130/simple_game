package ui;/*
Thierry Bergeron
Date : 2019-11-14
Description :
Nom du Fichier : UIObjects.GameProperties.java
*/

public class PanelProperties {
    //Game Board Size
    public final static int GAME_BOARD_WIDTH = 550;
    public final static int GAME_BOARD_HEIGHT = 400;

    //Initial Background Image position
    public final static int BACKGROUND_IMAGE_INITIAL_POSITION_X = 0;
    public final static int BACKGROUND_IMAGE_INITIAL_POSITION_Y = 0;

    //Timer delay for the frequency of updates/refresh
    public final static int TIMER_DELAY = 15;

    //PlayerHealth Display
    public final static int PLAYER_HEALTH_WIDTH = 200;
    public final static int PLAYER_HEALTH_HEIGHT = 25;

    //Score increment
    public final static int SCORE_INCREMENT = 1000;

}
