/*
Thierry Bergeron
Date : 2019-11-14
Description :
Nom du Fichier : PlayerScore.java
*/
package ui;

import javax.swing.*;

/**
 * Player Score display Panel
 */
public class ScorePanel extends JPanel {
    private JLabel scoreArea;

    /**
     * Constructor for the score panel
     */
    public ScorePanel(){
        scoreArea = new JLabel();
        add(scoreArea);
        scoreArea.setText("SCORE : ");
    }

    /**
     * function called to update the score
     * @param score changes the score value to be displayed
     */
    public void updateScore(int score){
        scoreArea.setText("SCORE : " + score);
    }
}