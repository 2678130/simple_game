package ui;

import javax.swing.*;
import java.awt.*;

/**
 * Game Class
 */
public class MainFrame extends JFrame {

    private MainPanel mainPanel;

    public MainFrame(MainPanel mainPanel) {
        this.mainPanel = mainPanel;
        initUI();
    }

    /**
     * Initialize the user interface (JFrame)
     */
    private void initUI() {
        //UI Properties
        this.setTitle("Devoir 2");
        this.setSize(600, 600);

        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setBackground(Color.blue);
        this.setLayout(null);
        this.setFocusable(true);


        this.add(this.mainPanel);
        this.mainPanel.setSize(600, 600);
        this.mainPanel.setLocation(0, 100);
        this.mainPanel.setVisible(true);
    }
}