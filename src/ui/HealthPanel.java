/*
Thierry Bergeron
Date : 2019-11-14
Description :
Nom du Fichier : PlayerHealth.java
*/
package ui;

import images.GameImages;

import javax.swing.*;
import java.awt.*;

/**
 * Player Health Display Panel
 */
public class HealthPanel extends JPanel {
    private Image backgroundImage;
    private int xValue;

    public HealthPanel(){
        ImageIcon iconBackgroundImage = new ImageIcon(GameImages.HEALTH_BAR_IMAGE_SRC_PATH);
        this.backgroundImage = iconBackgroundImage.getImage();
        this.xValue = -74; // 74 full health -- 244 dead
    }

    private void paintBackground(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.drawImage(this.backgroundImage, xValue, 0, this);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        paintBackground(g);
        Toolkit.getDefaultToolkit().sync();
    }

    public void updateBar(int playerHealth, int initialHealth){
        double percentage = (double)playerHealth /initialHealth;
        int value = (int)((1-percentage)*(170));
        this.xValue = -(value +74);
        repaint();
    }
}
