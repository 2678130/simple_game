/*
Thierry Bergeron
Date : 2019-11-14
Description :
Nom du Fichier : UIObjects.GameProperties.java
*/
package objects;

public class ObjectProperties {
    // Game coordinate boundaries (32 for 32 pixel object)
    final static int LOWER_BOUNDARY_32 = 367;   //(y)
    final static int UPPER_BOUNDARY_32 = 0;     //(y)
    final static int LEFT_BOUNDARY_32 = -5;     //(x)
    final static int RIGHT_BOUNDARY_32 = 524;   //(x)

    // Player initial properties (spawning position)
    final static int INITIAL_POSITION_X_PLAYER = 255;
    final static int INITIAL_POSITION_Y_PLAYER = 170;
    final static int INITIAL_HEALTH = 100;

    //Treasure properties
    final static int INITIAL_OFFSET = 25;
    final static int GAME_BOARD_WIDTH = 550; //for offset
    final static int GAME_BOARD_HEIGHT = 400; //for offset
    final static int TREASURE_RADIUS = 8;

    //Player properties
    public final static int PLAYER_RADIUS = 8;
    public final static int PLAYER_SPEED = 3;
    public final static int PLAYER_ACCELERATION = 1;

    //Object Properties
    final static int HIDDEN_POSITION_X = -50;
    final static int HIDDEN_POSITION_Y = -50;



    //Villain properties
    final static int INITIAL_VILLAIN_HEALTH = 20;
    public final static int VILLAIN_HIT_VALUE = 5;

    final static int VILLAIN_MOVEMENT_SPEED = 1;
    final static int VILLAIN_RADIUS = 6;

    final static int INITIAL_POSITION_X_VILLAIN_LEFT = -50;
    final static int INITIAL_POSITION_X_VILLAIN_RIGHT = 557;
    final static int INITIAL_POSITION_Y_VILLAIN_UPPER = -50;
    final static int INITIAL_POSITION_Y_VILLAIN_LOWER = 400;

    //Trap properties
    final static int TRAP_COLLIDED_IMAGE_DISPLAY_TIME = 700;
    final static int TRAP_COLLISION_RADIUS = 9;
    public final static int TRAP_HIT_VALUE = 5;
}