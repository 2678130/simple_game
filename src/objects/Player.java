package objects;/*
Thierry Bergeron
Date : 2019-11-14
Description : BoardObjects.Player object with it's properties
File Name : BoardObjects.Player.java
Source : http://zetcode.com/tutorials/javagamestutorial/movingsprites/
*/

import java.awt.event.KeyEvent;

/**
 * BoardObjects.Player object
 * <p>
 * -position property
 * <p>
 * -movement methods
 */
public class Player {

    //Position variables
    private int dx;
    private int dy;
    private int x;
    private int y;
    private boolean[] movementTable;

    //BoardObjects.Player properties
    private int health;

    //BoardObjects.Player Area for collisions
    private int playerAttackRadiusVariation;
    private int playerAttackRadius;


    /**
     * Initiate the player class
     */
    public Player() {
        x = ObjectProperties.INITIAL_POSITION_X_PLAYER;
        y = ObjectProperties.INITIAL_POSITION_Y_PLAYER;
        health = ObjectProperties.INITIAL_HEALTH;
        movementTable = new boolean[4];
    }

    /**
     * Method to get the player X axis position
     *
     * @return the x value of the player position
     */
    public int getPositionX() {
        return this.x;
    }

    /**
     * Method to get the player Y axis position
     *
     * @return the y value of the player position
     */
    public int getPositionY() {
        return this.y;
    }

    /**
     * Method to get the player radius to check for collisions
     *
     * @return player radius
     */
    public int getRadius() {
        return ObjectProperties.PLAYER_RADIUS;
    }

    /**
     * Method to get the current player health
     *
     * @return player health
     */
    public int getHealth() {
        return this.health;
    }

    /**
     * Method to move player
     * Need a boundary function?
     */
    public void movePlayer() {
        if ((x + dx) > ObjectProperties.LEFT_BOUNDARY_32 && (x + dx) < ObjectProperties.RIGHT_BOUNDARY_32) {
            this.x += dx;
        }
        if ((y + dy) > ObjectProperties.UPPER_BOUNDARY_32 && (y + dy) < ObjectProperties.LOWER_BOUNDARY_32) {
            this.y += dy;
        }
    }

    /**
     * Method to change the player health value
     *
     * @param value amount to be changed
     */
    public void modifyPlayerHealth(int value) {
        this.health -= value;
    }

    /**
     * Method to change the player attack radius when space bar is activated
     */
    public void changePlayerAttackRadius() {
        playerAttackRadius += playerAttackRadiusVariation;
    }

    /**
     * Tried using acceleration but gameplay was quite bad
     */
    private void playerAcceleration() {
        if (movementTable[0] ^ movementTable[1]) {
            if (movementTable[0]) {
                dy = -ObjectProperties.PLAYER_SPEED;
                /*if (dy > -ObjectProperties.PLAYER_SPEED) {
                    dy -= ObjectProperties.PLAYER_ACCELERATION;
                }*/
            } else {
                dy = ObjectProperties.PLAYER_SPEED;
                /*
                if (dy < ObjectProperties.PLAYER_SPEED) {
                    dy += ObjectProperties.PLAYER_ACCELERATION;
                }*/
            }
        } else {
            dy = 0;
        }
        //right - left
        if (movementTable[2] ^ movementTable[3]) {
            if (movementTable[2]) {
                dx = ObjectProperties.PLAYER_SPEED;/*
                if (dx < ObjectProperties.PLAYER_SPEED) {
                    dx += ObjectProperties.PLAYER_ACCELERATION;
                }*/
            } else {
                dx = -ObjectProperties.PLAYER_SPEED;
                /*
                if (dx > -ObjectProperties.PLAYER_SPEED) {
                    dx -= ObjectProperties.PLAYER_ACCELERATION;
                }*/
            }
        } else {
            dx = 0;
        }
    }

    /**
     * Method to take action when a keyboard event is triggered
     * - move player position
     *
     * @param e Keyboard Event
     */
    public void keyPressed(KeyEvent e) {
        //function to animate player when key is pressed
        //get the key code value
        int key = e.getKeyCode();
        //Movement Keys
        if (key == KeyEvent.VK_UP) {
            //dy = -2;
            movementTable[0] = true;
        }
        if (key == KeyEvent.VK_DOWN) {
            //dy = 2;
            movementTable[1] = true;
        }
        if (key == KeyEvent.VK_RIGHT) {
            //dx = 2;
            movementTable[2] = true;
        }
        if (key == KeyEvent.VK_LEFT) {
            //dx = -2;
            movementTable[3] = true;
        }
        //Attack key
        if (key == KeyEvent.VK_SPACE) {
            playerAttackRadiusVariation = 10;
        }
        playerAcceleration();
    }

    /**
     * Method to move and animate the player given user input
     *
     * @param e event
     */
    public void keyReleased(KeyEvent e) {
        //set key code
        int key = e.getKeyCode();

        //movement keys
        if (key == KeyEvent.VK_UP) {
            movementTable[0] = false;
        }
        if (key == KeyEvent.VK_DOWN) {
            movementTable[1] = false;
        }
        if (key == KeyEvent.VK_RIGHT) {
            movementTable[2] = false;
        }
        if (key == KeyEvent.VK_LEFT) {
            movementTable[3] = false;
        }

        //Attack key
        if (key == KeyEvent.VK_SPACE) {
            playerAttackRadiusVariation = -10;
        }
        playerAcceleration();
    }
}
