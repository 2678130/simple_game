package objects;/*
Thierry Bergeron
Date : 2019-11-14
Description :
Nom du Fichier : BoardObjects.Traps.java
*/

import java.util.Random;

public class Trap {
    private int x;
    private int y;
    private int collisionTime;
    private Random random;

    private boolean collided;
    private boolean active;
    private boolean activated;
    private boolean show;

    public Trap(Random random){
        this.x = -50;
        this.y = -50;
        this.random = random;
        this.collided = false;
        this.active = false;
        this.activated = false;
    }

    public void setActive(){
        this.active = !this.active;
        this.activated = true;
    }

    /**
     * Set a random position when trap is activated from the controller function
     * runs the function every time timer is activated, if time is too long the trap will disappear after randomized time
     * @param gameTime denotes the game time when trap was activated
     */
    public void updatePosition(int gameTime){
        if(this.activated){
            this.x = random.nextInt(ObjectProperties.GAME_BOARD_WIDTH - (ObjectProperties.INITIAL_OFFSET * 2)) + ObjectProperties.INITIAL_OFFSET;
            this.y = random.nextInt(ObjectProperties.GAME_BOARD_HEIGHT - (ObjectProperties.INITIAL_OFFSET * 2)) + ObjectProperties.INITIAL_OFFSET;
            this.activated = false;
        }
        updateCollision(gameTime);
    }

    public int getPositionX(){
        return this.x;
    }

    public int getPositionY(){
        return this.y;
    }

    public boolean isCollided(){
        return this.collided;
    }

    public boolean isActive(){
        return this.active;
    }

    public void collisionDetected(int gameTime){
        this.collisionTime = gameTime;
        this.collided = true;
        this.active = false;
        this.show = true;
    }

    public int getRadius(){
        return ObjectProperties.TRAP_COLLISION_RADIUS;
    }

    public boolean isShow(){
        return this.show;
    }

    //-------------------------------------------------- PRIVATE ------------------------------------------------------------------

    private void updateCollision(int gameTime){
        if(collided && gameTime > (this.collisionTime + ObjectProperties.TRAP_COLLIDED_IMAGE_DISPLAY_TIME)){
            this.x = -50;
            this.y = -50;
            this.collided = false;
            this.show = false;
        }
    }

}
