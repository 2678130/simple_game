package objects;/*
Thierry Bergeron
Date : 2019-11-14
Description :
Nom du Fichier : BoardObjects.Villain.java
*/

import java.util.Random;

/**
 * BoardObjects.Villain Object
 */
public class Villain {
    //Position variables
    private int dx;
    private int dy;
    private int x;
    private int y;

    //Villain properties
    //private int health = ObjectProperties.INITIAL_VILLAIN_HEALTH;
    private boolean active;

    //Random Object
    private Random random;

    //State
    private long gameTime;
    private boolean firstCollision;
    private boolean collision;
    private long collisionTime;

    //Initiate villain object
    // ? could be passed int the random object created in another class
    // ? do we need a time object?
    public Villain(long seed) {
        this.random = new Random(seed);
        this.collision = false;
        this.firstCollision = true;
        this.x = -50;
        this.y = -50;
        spawnVillain();
    }

    //Spawn function

    /**
     * set the initial position of villain creation
     */
    private void spawnVillain() {
        int position = this.random.nextInt(4) + 1;
        switch (position) {
            // upper left
            case 1:
                this.x = ObjectProperties.INITIAL_POSITION_X_VILLAIN_LEFT;
                this.y = ObjectProperties.INITIAL_POSITION_Y_VILLAIN_UPPER;
                break;
            // upper right
            case 2:
                this.x = ObjectProperties.INITIAL_POSITION_X_VILLAIN_RIGHT;
                this.y = ObjectProperties.INITIAL_POSITION_Y_VILLAIN_UPPER;
                break;
            // lower left
            case 3:
                this.x = ObjectProperties.INITIAL_POSITION_X_VILLAIN_LEFT;
                this.y = ObjectProperties.INITIAL_POSITION_Y_VILLAIN_LOWER;
                break;
            // lower right
            case 4:
                this.x = ObjectProperties.INITIAL_POSITION_X_VILLAIN_RIGHT;
                this.y = ObjectProperties.INITIAL_POSITION_Y_VILLAIN_LOWER;
                break;
        }
    }

    //Movement function
    //Attack player then bounce off player after attack
    //Move towards player

    /**
     * Function to move villain towards the player
     * @param playerX get the player x position value
     * @param playerY get the player y position value
     */
    public void moveVillain(int playerX, int playerY, long gameTime) {
        this.gameTime = gameTime;
        if(this.active && !this.collision){
            this.dx = getCloser(this.x, playerX);
            this.dy = getCloser(this.y, playerY);
            this.x += this.dx;
            this.y += this.dy;
        } else if (this.active) {
            bounceVillain(playerX, playerY);
        }
    }

    /**
     * Function to be called when a collision is detected
     * @param gameTime need game time to control delays
     */
    public void collisionDetected(long gameTime){
        if(this.firstCollision){
            this.collision = true;
            this.collisionTime = gameTime;
            this.firstCollision = false;
        }
    }

    /**
     * Movement function when a collision is detected
     * TO BE CHANGED, would prefer slope to calculate bounce off
     * @param playerX player x position
     * @param playerY player y position
     */
    private void bounceVillain(int playerX, int playerY){
        if(this.gameTime < collisionTime + 4){
            this.dx = getFurther(this.x, playerX)*5;
            this.dy = getFurther(this.y, playerY)*5;
            this.x += dx;
            this.y += dy;
        } else {
            //reset the collision
            this.collision = false;
            this.firstCollision = true;
        }
    }

    /**
     * function to disable movement of villain, time when to activate
     * @param value activate or deactivate villain
     */
    public void setActive(boolean value){
        this.active = value;
    }

    /**
     * get villain x position
     * @return x position
     */
    public int getX() {
        return this.x;
    }

    /**
     * get villain y position
     * @return y position
     */
    public int getY(){
        return this.y;
    }

    /**
     * function to know where to move villain, either up or down, left or right, called by the moveVillain function
     * @param a villain x value
     * @param b player x value
     * @return direction to be moved
     */
    private int getCloser(int a, int b) {
        int movement = 0;
        if (a > b) {
            movement -= (ObjectProperties.VILLAIN_MOVEMENT_SPEED);
        } else {
            movement += (ObjectProperties.VILLAIN_MOVEMENT_SPEED);
        }
        return movement;
    }

    /**
     * function to move villain away from player when there is a collision
     * @param a villain x value
     * @param b player x value
     * @return direction and amount to be moved
     */
    private int getFurther(int a, int b) {
        int movement = 0;
        if (a < b) {
            movement -= (ObjectProperties.VILLAIN_MOVEMENT_SPEED);
        } else {
            movement += (ObjectProperties.VILLAIN_MOVEMENT_SPEED);
        }
        return movement;
    }

    /**
     * returns the radius value of villain to be measured vs player value
     * @return the radius value
     */
    public int getVillainRadius(){
        return ObjectProperties.VILLAIN_RADIUS;
    }

    public void respawnVillain(){
        this.spawnVillain();
    }

    public boolean isActive(){
        return this.active;
    }
}
