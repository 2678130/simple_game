package objects;/*
Thierry Bergeron
Date : 2019-11-14
Description :
Nom du Fichier : TreasureTraps.java
*/

import java.util.Random;

public class Treasure {

    //object position
    private int x;
    private int y;

    //seed for generation and random object
    private long seed;
    private Random random;

    //state variables
    private int collisionTime;
    private boolean collided;


    public Treasure() {
        autoInitTreasureTraps();
    }

    /**
     * Auto generation of random (system current time millis)
     */
    private void autoInitTreasureTraps() {
        this.seed = System.currentTimeMillis();
        this.setRandom();
        this.collided = false;
        this.collisionTime = 0;
        this.x = -50;
        this.y = -50;
    }

    public void spawnTreasure(){
        this.resetPosition();
    }

    /**
     * Manually set the seed value, it will be used to generate the initial location of the treasures
     *
     * @param seedValue
     */
    public void setSeed(long seedValue) {
        seed = seedValue;
    }

    /**
     * Function to set the seed of the random object for generating the position of the treasure
     */
    public void setRandom() {
        random = new Random(seed);
    }

    /**
     * Function to generate a new random position
     */
    private void resetPosition() {
        x = random.nextInt(ObjectProperties.GAME_BOARD_WIDTH - (ObjectProperties.INITIAL_OFFSET * 2)) + ObjectProperties.INITIAL_OFFSET;
        y = random.nextInt(ObjectProperties.GAME_BOARD_HEIGHT - (ObjectProperties.INITIAL_OFFSET * 2)) + ObjectProperties.INITIAL_OFFSET;
    }

    /**
     * Function to be called repeatedly to update Treasure state
     * @param gameTime update game time
     */
    public void updateState(int gameTime) {
        if(this.collided){
            if(gameTime > this.collisionTime + 200){
                this.resetPosition();
                this.collided = false;
            }
        }
    }

    /**
     * Function to be called when a collision is detected, treasure is temp hidden out of place
     * @param gameTime need game time to know how long to hide treasure
     */
    public void collisionDetected(int gameTime) {
        this.x = -50;
        this.y = -50;
        this.collided = true;
        this.collisionTime = gameTime;
    }

    /**
     * Function to get the x axis position of the generated treasure
     *
     * @return x axis coordinate
     */
    public int getPositionX() {
        return x;
    }

    /**
     * Function to get the y axis position of the generated treasure
     *
     * @return y axis coordinate
     */
    public int getPositionY() {
        return y;
    }

    /**
     * function to get the size of the radius of the treasure used to detect collision
     * * modified in ObjectProperties Class
     *
     * @return treasure radius
     */
    public int getTreasureRadius() {
        return ObjectProperties.TREASURE_RADIUS;
    }
}
