// ===========================================================================
// Programmeur : T. Bergeron
// Fichier : 
// Description : 
// Date : 2019
// ===========================================================================
package objects;

public abstract class GameObject {
    private int positionX;
    private int positionY;
    private int radius;
    private int health;

    private boolean active;
    private boolean activated;

    public GameObject(int positionX, int positionY, int radius, boolean active, boolean activated) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.radius = radius;
        this.active = active;
        this.activated = activated;
    }

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public int getRadius() {
        return radius;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }
}