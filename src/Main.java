/*
Thierry Bergeron
Date : 2019-11-14
Description :
Nom du Fichier : Main.java
*/

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(Controller::new);
    }
}