/*
Thierry Bergeron
Date : 2019-11-22
Description :
Nom du Fichier : Controller.java
*/


import objects.*;
import ui.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Random;

public class Controller implements ActionListener {

    /**
     *
     */
    private BoardPanel boardPanel;
    private HealthPanel healthBoard;
    private ScorePanel scoreBoard;
    private JButton startButton;
    private JButton stopButton;

    //--- Game Objects (Data) ---
    private Player player = new Player();
    private Villain[] villain;
    private Treasure treasure;
    private Trap[] trap;
    private Random random;

    //--- State Variables ---
    private Timer timer;
    private int gameTime;
    private int score;
    private int initialHealth;
    private int trapInterval;
    private int villainInterval;
    private final int numberOfTraps = 100;
    private final int numberOfVillains = 4;


    public Controller() {
        this.initGame();
    }

    //---------------------------------- INITIALIZATION FUNCTION -------------------------------

    /**
     * Initialization function
     */
    private void initGame() {
        this.random = new Random();
        this.villain = new Villain[this.numberOfVillains];
        this.createVillains();
        this.trap = new Trap[this.numberOfTraps];
        this.createTraps();
        this.treasure = new Treasure();
        this.trapInterval = 0;

        this.timer = new Timer(PanelProperties.TIMER_DELAY, this);

        this.boardPanel = new BoardPanel(this.player, this.villain, this.treasure, this.trap);
        this.healthBoard = new HealthPanel();
        this.scoreBoard = new ScorePanel();
        this.startButton = new JButton();
        this.startButton.addActionListener(this);
        this.stopButton = new JButton();
        this.stopButton.addActionListener(this);

        MainPanel mainPanel = new MainPanel(this.boardPanel, this.healthBoard, this.scoreBoard, this.startButton, this.stopButton);
        MainFrame mainFrame = new MainFrame(mainPanel);
        mainFrame.addKeyListener(new KeyboardListener());
    }

    /**
     * Create the villains in the villain array
     */
    private void createVillains() {
        for (int i = 0; i < this.villain.length; i++) {
            long seed = this.random.nextInt(27348);
            this.villain[i] = new Villain(seed);
        }
    }

    /**
     * Create the traps in the trap array
     */
    private void createTraps() {
        for (int i = 0; i < this.trap.length; i++) {
            this.trap[i] = new Trap(this.random);
        }
    }

    //------------------------------------- BUTTON CONTROL FUNCTIONS--------------------------------

    /**
     * Start Game function called when start button is clicked
     */
    private void startGame() {
        this.initialHealth = this.player.getHealth();
        this.score = 0;
        this.gameTime = 0;
        this.timer.start();
        this.startButton.setVisible(false);
        this.stopButton.setVisible(true);
    }

    /**
     * Stop Game function called when stop button is clicked or player dies
     */
    private void stopGame() {
        this.timer.stop();
        this.startButton.setVisible(true);
        this.stopButton.setVisible(false);
        this.initGame();
    }


    //-------------------------------- UPDATES ------------------------------//

    /**
     * Update the BoardPanel display by forcing a repaint
     * Update the health board by calling a function and pushing the new player health
     * Update the score by calling a function to update the score
     */
    private void updateView() {
        this.boardPanel.repaint();
        this.healthBoard.updateBar(this.player.getHealth(), this.initialHealth);
        this.scoreBoard.updateScore(this.score);
    }

    /**
     * Force the update of the player, villain, traps.
     */
    private void updateModel() {
        this.player.movePlayer();
        this.updateVillains();
        this.updateTraps();
        this.treasure.updateState(this.gameTime);
    }

    /**
     * Update the controller state of the game
     * - score is updated directly from the inner function treasureCollisionDetection
     * - player health is updated directly from the inner function villainCollisionDetection
     * -
     */
    private void updateState() {
        this.gameTimeEvents();
        this.treasureCollisionDetection();
        if (this.player.getHealth() <= 0) {
            this.stopGame();
        }
        this.gameTime += 1;
    }

    //-------------------------------------- EVENT HANDLING -------------------------------------------//

    /**
     * Event listener to be activated every time a timer event is triggered or a startButton or stopButton event
     *
     * @param e event
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.timer) {
            updateState();
            updateModel();
            updateView();
        }
        else if (e.getSource() == this.startButton) {
            startGame();
        }
        else if (e.getSource() == this.stopButton) {
            stopGame();
        }
    }

    /**
     * Keyboard listener inner class directly calls the player internal functions to update there acceleration
     */
    private class KeyboardListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            super.keyPressed(e);
            player.keyPressed(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            super.keyReleased(e);
            player.keyReleased(e);
        }
    }
    //-------------------------------- GAME TIME MANAGEMENT --------------------------------------------//

    /**
     * Function that controls the Game Events such as spawning of villains and traps
     */
    private void gameTimeEvents() {

        if (this.gameTime == 200) {
            this.treasure.spawnTreasure();
        }

        if (this.gameTime > 200 && gameTime % 500 == 0) {
            if (this.villainInterval == this.numberOfVillains) {
                this.villainInterval = 0;
            }
            activateVillains(this.villainInterval);
            this.villainInterval += 1;
        }

        // every 200 gameTime updates a new trap is activated (100 in the array), will loop through then start again @ 1
        if (this.gameTime % 200 == 0) {
            if (this.trapInterval == this.numberOfTraps) {
                this.trapInterval = 0;
            }
            activateTraps(trapInterval);
            this.trapInterval += 1;
        }

    }

    //--------------------------- PRIVATE CONTROLLER FUNCTIONS -----------------------------------------//

    /**
     * Function to move villains and update there location
     */
    private void updateVillains() {
        for (int i = 0; i < villain.length; i++) {
            if (villain[i].isActive()) {
                villainCollisionDetection(i);
                villain[i].moveVillain(player.getPositionX(), player.getPositionY(), gameTime);
            }
        }
    }

    /**
     * function to update the traps
     * - checks for collision using the trap collision detection function
     * - if collided changes the state of the object from collided: false to collided: true (internal of the trap collision detection)
     * - calls the update position class function to update the position (has internal delay to hide trap once it's triggered after ObjectProperties.TRAP_COLLIDED_IMAGE_DISPLAY_TIME)
     */
    private void updateTraps() {
        for (int i = 0; i < this.trap.length; i++) {
            if (trap[i].isActive()) {
                trapCollisionDetection(i);
                this.trap[i].updatePosition(this.gameTime);
            }
            else if (trap[i].isShow()) {
                this.trap[i].updatePosition(this.gameTime);
            }
        }
    }

    /**
     * Function to activate villains
     */
    private void activateVillains(int i) {
        if (!villain[i].isActive()) {
            villain[i].setActive(true);
        }
        else {
            villain[i].respawnVillain();
        }
    }

    /**
     * Function to set the trap actives
     *
     * @param i the i parameter is used for the index of the Trap Array Object when called by the GAME MANAGEMENT FUNCTION
     */
    private void activateTraps(int i) {
        trap[i].setActive();
    }

    /**
     * Function to detect collision between villains and player
     *
     * @param i variable to iterate through villain array
     */
    private void villainCollisionDetection(int i) {
        if (collisionDetection(player.getPositionX(), player.getPositionY(), player.getRadius(), villain[i].getX(), villain[i].getY(), villain[i].getVillainRadius())) {
            this.villain[i].collisionDetected(this.gameTime);
            this.player.modifyPlayerHealth(ObjectProperties.VILLAIN_HIT_VALUE);
        }
    }

    /**
     * Function to detect collision between villains and player
     *
     * @param i variable to iterate through villain array
     */
    private void trapCollisionDetection(int i) {
        if (collisionDetection(player.getPositionX(), player.getPositionY(), player.getRadius(), trap[i].getPositionX(), trap[i].getPositionY(), trap[i].getRadius())) {
            this.trap[i].collisionDetected(this.gameTime);
            this.player.modifyPlayerHealth(ObjectProperties.TRAP_HIT_VALUE);
        }
    }

    /**
     * Function to detect collision between player and treasure
     */
    private void treasureCollisionDetection() {
        if (collisionDetection(player.getPositionX(), player.getPositionY(), player.getRadius(), treasure.getPositionX(), treasure.getPositionY(), treasure.getTreasureRadius())) {
            this.treasure.collisionDetected(this.gameTime);
            this.score += PanelProperties.SCORE_INCREMENT;
        }
    }


    /**
     * Function to get the distance between coordinates, used for collision detection
     * formula : distance = squareRoot ( (x1 - x2)^2 + (y1 - y2)^2 )
     *
     * @param ax x axis coordinate of object A
     * @param ay y axis coordinate of object A
     * @param bx x axis coordinate of object B
     * @param by y axis coordinate of object B
     * @return distance between Object A and B
     */
    private int distanceBetweenTwoObjects(int ax, int ay, int bx, int by) {
        double distance = Math.sqrt(Math.pow((ax - ay), 2) + Math.pow((bx - by), 2));
        return (int) distance;
    }

    /**
     * Function to detect collision between 2 objects
     *
     * @param x1      x axis coordinate of object 1
     * @param y1      y axis coordinate of object 1
     * @param radius1 hit radius of object 1
     * @param x2      x axis coordinate of object 2
     * @param y2      y axis coordinate of object 2
     * @param radius2 hit radius of object 2
     * @return if there is a collision or not
     */
    private boolean collisionDetection(int x1, int y1, int radius1, int x2, int y2, int radius2) {
        boolean collisionDetected = false;
        int distance = distanceBetweenTwoObjects(x1, x2, y1, y2);
        if (distance < (radius1 + radius2)) {
            collisionDetected = true;
        }
        return collisionDetected;
    }
}
