/*
Thierry Bergeron
Date : 2019-11-21
Description :
Nom du Fichier : Images.java
*/
package images;

public class GameImages {
    public final static String VILLAIN_IMAGE_SRC_PATH = "src/images/Villain_1.png";
    public final static String PLAYER_IMAGE_SRC_PATH = "src/images/player.png";
    public final static String TREASURE_IMAGE_SRC_PATH = "src/images/Treasure.png";
    public final static String LEVEL1_IMAGE_SRC_PATH = "src/images/level1.png";
    public final static String HEALTH_BAR_IMAGE_SRC_PATH = "src/images/health_bar.png";

    public final static String TRAP_CLOSED_IMAGE_SRC_PATH = "src/images/closedtrap.png";
    public final static String TRAP_OPEN_IMAGE_SRC_PATH = "src/images/opentrap.png";

}